About my Sochi's experience

Travail de développement Frontend.

À propos :

    Je n'ai malheureusement pas réussi à terminer et à optimiser comme 
    il faut la maquette.
    
    J'ai beaucoup appris, mais j'ai rencontré de nombreuses difficultés.
    
    La première est que j'ai démarré ce projet en ayant beaucoup moins de 
    connaissance que maintenant. J'ai compris à quel point le démarre d'un 
    projet est important.
    
    La deuxième est que je manque d'aisance avec les médias query et le
    responsive. 
    
    Pour finir, je pense avoir mal géré le début et cela m'a énormément pénalisé
    durant la progression. Enormément de conflit avec ma première partie et la 
    seconde.
    
À savoir :

    Je n'ai pas terminé la page des détails des chambres.
    
    Je n'ai pas optimisé le responsive.
    
    J'ai utilisé des images génériques qui changent parfois la couleur de la 
    police par rapport à la maquette.
    
    Par manque de temps, je n'ai pas eu le temps de bien faire la NAV car je 
    pensais faire en JS afin d'améliorer le switch en scrollant.
    
    Quelques petits éléments manquent ou sont pas bien fait car je n'y arrivais
    pas.
    
Conclusion : 

    Je pense m'être beaucoup amélioré et j'ai pris du plaisir à faire cet 
    exercice et j'espère pouvoir améliorer les points qui ont pêchés durant.
    
    
    
